# x-docker framework

**xd** is a _Work-In-Progress_ tool for creating and starting independent and isolated desktop environments using docker.

The idea behind it is to be able to have several desktops, such as:

1. For standard browsing and social networks
2. For development
3. For privacy (using TOR)

So, something like `xd -r dev1` should be enough for launching a full development desktop on TTY 2 and `xd -r tor -e -t -v3` for launching an encrypted TOR-enabled desktop on TTY 3.

## Usage

```
./xd -h
Usage: ./xd <-r|c> TAG|TAG/FLAVOR [other options]
Options:
-r <tag> 	 run image (created with -c)
-l       	 list ready-to-use compiled images
-f       	 list available flavors
-c <tag/flavor>	 create image with <tag> based on flavor <flavor>
-u <user>	 launch image with internal user <user> (must exist in host system)
-d <dir> 	 use directory <dir> as persistent home
-t       	 use TOR as networking stack
-e       	 use encryption for home directory
-v <vt>  	 lauch Xorg server on TTY <vt> (default 2)
-x <cmd> 	 execute <cmd> as entry point for docker instead of image default
```

List existing flavors

```
./xd -f
full
```

Create a new image based on flavor `full`. You can create new flavors on directory flavors/.

```
./xd -c dev1/full
[+] Building 1.3s (7/7) FINISHED                                                                                                                                                  
 => [internal] load build definition from Doc
 => => transferring dockerfile: 2.31kB
 ....
 => => naming to docker.io/library/xd:dev1
```

List created images

```
./xd -l 
dev1
```

Launch docker desktop, use home encryption

```
./xd -r dev1 -e
creating new home
using home directory /root/.xd/xddev1
using encryption for persistent home
Passphrase:
....
```

